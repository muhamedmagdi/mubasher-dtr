
// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'peers performance'
    },

    xAxis: {
        type: 'category'
    },
    legend: {
        enabled: true
    },
    plotOptions: {
        series: {
            name : 'peers performance' ,
            borderWidth: 0,
            dataLabels: {
                enabled: false,

            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    series: [{
        colorByPoint: true,
        data: [{
            name: 'ciab',
            y: 10,
            drilldown: 'CIAB' ,
            color: '#70D6E4'
        }, {
            name: 'ABC',
            y: 12,
            drilldown: 'ABC',
            color: '#70D6E4'
        }, {
            name: 'comi',
            y: 20.02,
            drilldown: 'COMI' ,
            color: '#5BD075'
        }, {
            name: 'RREI',
            y: 12,
            drilldown: 'RREI' ,
            color: '#70D6E4'
        }, {
            name: 'CIRA',
            y: 9,
            drilldown: 'CIRA',
            color: '#70D6E4'
        }, {
            name: 'OBIR',
            y: 8,
            drilldown: 'OBIRA',
            color: '#70D6E4'
        }]
    }],
    drilldown: {
        series: [{
            name: 'Microsoft Internet Explorer',
            id: ' mohamed',

        }, {
            name: 'Chrome',
            id: 'Chrome',
            data: [
                [
                    'v40.0',
                    5
                ],
                [
                    'v41.0',
                    4.32
                ],
                [
                    'v42.0',
                    3.68
                ],
                [
                    'v39.0',
                    2.96
                ],
                [
                    'v36.0',
                    2.53
                ],
                [
                    'v43.0',
                    1.45
                ],
                [
                    'v31.0',
                    1.24
                ],
                [
                    'v35.0',
                    0.85
                ],
                [
                    'v38.0',
                    0.6
                ],
                [
                    'v32.0',
                    0.55
                ],
                [
                    'v37.0',
                    0.38
                ],
                [
                    'v33.0',
                    0.19
                ],
                [
                    'v34.0',
                    0.14
                ],
                [
                    'v30.0',
                    0.14
                ]
            ]
        }, {
            name: 'Firefox',
            id: 'Firefox',
            data: [
                [
                    'v35',
                    2.76
                ],
                [
                    'v36',
                    2.32
                ],
                [
                    'v37',
                    2.31
                ],
                [
                    'v34',
                    1.27
                ],
                [
                    'v38',
                    1.02
                ],
                [
                    'v31',
                    0.33
                ],
                [
                    'v33',
                    0.22
                ],
                [
                    'v32',
                    0.15
                ]
            ]
        }, {
            name: 'Safari',
            id: 'Safari',
            data: [
                [
                    'v8.0',
                    2.56
                ],
                [
                    'v7.1',
                    0.77
                ],
                [
                    'v5.1',
                    0.42
                ],
                [
                    'v5.0',
                    0.3
                ],
                [
                    'v6.1',
                    0.29
                ],
                [
                    'v7.0',
                    0.26
                ],
                [
                    'v6.2',
                    0.17
                ]
            ]
        }, {
            name: 'Opera',
            id: 'Opera',
            data: [
                [
                    'v12.x',
                    0.34
                ],
                [
                    'v28',
                    0.24
                ],
                [
                    'v27',
                    0.17
                ],
                [
                    'v29',
                    0.16
                ]
            ]
        }]
    }
});
Highcharts.chart('circle', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'market depth'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
pie: {
    allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
        enabled: false
    },
    showInLegend: true
}
},
series: [{
    name :'market depth' ,
    colorByPoint: true,
    data: [{
        name: 'up',
        y: 60 ,
        color : ' #41BBEE'
    }, {
        name: 'down',
        y: 25,
        sliced: true,
        selected: true ,
        color : '#70D6E4'
    }, {
        name: 'unchanged',
        y: 15 ,
        color :'#E4E9EC'

    }]
}]
});
